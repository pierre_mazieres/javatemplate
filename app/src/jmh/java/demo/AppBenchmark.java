package demo;

import org.openjdk.jmh.annotations.Benchmark;

public class AppBenchmark {
    @Benchmark public static void appHasAGreeting() {
        App classUnderTest = new App();
        classUnderTest.getGreeting();
    }
}
