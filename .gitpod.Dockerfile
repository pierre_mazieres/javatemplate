FROM gitpod/workspace-full

RUN sudo apt update -y
RUN sudo apt install -y ruby ruby-dev
RUN echo export PATH="${PATH}:${HOME}/.local/bin" >> /home/gitpod/.bashrc
RUN sudo apt install -y zip unzip curl
RUN curl -s "https://get.sdkman.io" | bash
